
/*
  From Dan Shiffman Particle System: https://processing.org/examples/simpleparticlesystem.html
*/

// A class to describe a group of Particles
class ParticleSystem {
  ArrayList<Particle> particles;
  PVector origin;

  ParticleSystem(PVector position) {
    this.origin = position.copy();
    this.particles = new ArrayList<Particle>();
  }
  
  // check if all particles have died
  boolean systemIsDead() {
    // check all particles
    for (Particle p : this.particles) {
      if (!p.isDead()) {
        return false;
      }
    }
    
    return true;
  }
  
  void move(PVector vel) {
    this.origin.add(vel);
  }
  
  // reset the particle system
  void reset() {
    this.particles.clear(); 
  }

  void addParticle(float lifespan) {
    this.particles.add(new Particle(origin, lifespan));
  }

  void run() {
    for (int i = this.particles.size()-1; i >= 0; i--) {
      Particle p = particles.get(i);
      p.run();
      if (p.isDead()) {
        this.particles.remove(i);
      }
    }
  }
}

// object for each particle in system
class Particle {
  PVector position;
  PVector velocity;
  PVector acceleration;
  float lifespan;

  Particle(PVector l, float startingLifespan) {
    acceleration = new PVector(0, 0.005);
    velocity = new PVector(random(-0.5, 0.5), random(-1, 0));
    position = l.copy();
    lifespan = startingLifespan;
  }

  void run() {
    update();
    display();
  }

  // Method to update position
  void update() {
    velocity.add(acceleration);
    position.add(velocity);
    lifespan -= 1.0;
  }

  // Method to display
  void display() {
    fill(255, 255, 153, lifespan);
    ellipse(position.x, position.y, 4, 4);
  }

  // Is the particle still useful?
  boolean isDead() {
    if (lifespan < 0.0) {
      return true;
    } else {
      return false;
    }
  }
}
