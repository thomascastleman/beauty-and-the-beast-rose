
// calculate start of show and end of show
void startShow() {
  SHOW_START = getTimeSeconds();  // get the current time as start time
  SHOW_END = (SHOW_START + SHOW_DURATION); // % 86400;  // mod 86400 to circle back if show goes past midnight
  resetGraphics();  // reset all the rose graphics to defaults
  showStarted = true;  // register show has started
  fadeInStatus = 0;  // fade in on show start
}

// hard reset the rose to beginning of show state, fades in automatically
void resetGraphics() {
  showFlowerCover = true;  // show flower cover
  reconstructedRose = false;  // not reconstructed
  isFullyReconstructed = false;  // not fully reconstructed
  initializeGraphicsObjects();  // set up all the graphics
}

// setup all petal objects / end particle system
void initializeGraphicsObjects() {
  staticPetals.clear();
  stemPanels.clear();
  fallingPetals.clear();
  
  // initialize all inside static petals
  for (int i = 0; i < insideStaticPetalVertices.length; i++) {
    staticPetals.add(new StaticPetal(insideStaticPetalVertices[i], randomColor(liveRoseColors), randomColor(deadRoseColors), false));
  }
  
  // add all outside petals, which fade away by the end of the show
  for (int i = 0; i < outsideStaticPetalVertices.length; i++) {
    staticPetals.add(new StaticPetal(outsideStaticPetalVertices[i], randomColor(liveRoseColors), randomColor(deadRoseColors), true));
  }
  
  // add static petal objects for the stem panels
  for (int i = 0; i < stemVertices.length; i++) {
    stemPanels.add(new StaticPetal(stemVertices[i], randomColor(liveStemColors), randomColor(deadStemColors), false));
  }
  
  PVector flowerCenter = new PVector(0.0, 0.0);
  
  // sum positions of all petal centroids
  for (int i = 0; i < staticPetals.size(); i++) {
    flowerCenter.add(staticPetals.get(i).centroid);
  }
  
  // compute average centroid of all petals
  flowerCenter.x /= staticPetals.size();
  flowerCenter.y /= staticPetals.size();
  
  // adjust the center of particle system a bit specifically so it looks nice with this rose shape
  flowerCenter.add(25, 25);
  
  // create new particle system for post-reconstruction glow at center of screen
  endGlowPS = new ParticleSystem(flowerCenter);
}

// generate a randomly shaped, randomly colored falling petal object
FallingPetal getRandomFallingPetal() {
    // choose a random falling petal shape for this falling petal
  int i = (int) (Math.random() * fallingPetalVertices.length);
  float[][][] vertices = fallingPetalVertices[i];
  
  // construct a falling petal object in this shape
  return new FallingPetal(vertices);
}

// generally, get a random color from a color array
color randomColor(color[] colors) {
  return colors[(int) Math.floor(Math.random() * colors.length)];
}

// get the current time in seconds
int getTimeSeconds() {
  return second() + (60 * minute()) + (3600 * hour()); // (int) map(mouseX, 0, width, SHOW_START, SHOW_END);
}

// draw a shape with vertices relative to the center of the window
void drawVertices(float[][] vertices) {
  beginShape();
  for (float[] vertex : vertices) {
    vertex(vertex[0], vertex[1]);
  }
  endShape();
}

// copy an array of vertices
float[][] verticesCopy(float[][] v) {
  float[][] copy = new float[v.length][v[0].length];
  for (int i = 0; i < v.length; i++) {
    for (int j = 0; j < v[i].length; j++) {
      copy[i][j] = v[i][j];
    }
  }
  return copy;
}

// get the centroid coordinates of a given set of vertices
PVector getCentroid(float[][] vertices) {
    // get sums of X and Y values
    int sumX = 0, sumY = 0;
    for (int i = 0; i < vertices.length; i++) {
      sumX += vertices[i][0];
      sumY += vertices[i][1];
    }
    
    // calculate centroid point
    return new PVector((int) Math.floor(sumX / vertices.length), (int) Math.floor(sumY / vertices.length));
}

// ---- debug: tool for drawing things relative to center -- prints coordinates to console as mouse is clicked

ArrayList<int[]> lastVertices = new ArrayList<int[]>();

int c = 0;
void mousePressed() {
  int x = mouseX - CENTER_X;
  int y = mouseY - CENTER_Y;
  println("{" + x + ", " + y + "},");
  
  // add to last vertices drawn
  int[] coords = {x, y};
  lastVertices.add(coords);
  
  noStroke();
  fill(0);
  ellipse(mouseX, mouseY, 4, 4);
  // text(c++, mouseX, mouseY);
}

// separate shapes with space key
void keyPressed() {

  // --------------------------------------- DEBUG -------------------------------------------
  if (keyCode == 32) {
    println("},");
    println("{");
    
    fill(0, 255, 0);
    stroke(0);
    pushMatrix();
    translate(CENTER_X, CENTER_Y);
    beginShape();
    for (int[] v : lastVertices) {
      vertex(v[0], v[1]);
    }
    endShape(CLOSE);
    popMatrix();
    noStroke();
    
    // start new list of vertices
    lastVertices.clear();
  }
  
  //------------------------------------------ END DEBUG -----------------------------------------
  
  // if show started allow key command cues
  if (showStarted) {
    
    // cue petal fall with space key
    if (keyCode == 32) {
      // add falling petal
      fallingPetals.add(getRandomFallingPetal());
    }
    
    // cue Beast's death ("d" key)
    if (keyCode == 68) {
      // add the LAST falling petal
      fallingPetals.add(getRandomFallingPetal());
      
      // turn off the flower cover 
      showFlowerCover = false;
      
      // rose is not reconstructed, since it's shattering
      reconstructedRose = false;
      
      // rose is definitely not fully reconstructed--it's shattering
      isFullyReconstructed = false;
      
      // register that each petal is shattering, and randomize its velocity
      for (StaticPetal p : staticPetals) {
        p.isShattering = true;
        p.randomizeShatterVelocity();
      }
      
      // reset glow effect particle system
      endGlowPS.reset();
    }
  
    // cue the transformation ("t" key) (rose reconstructs, sparkles)
    if (keyCode == 84) {
      // register that each petal is not shattering anymore
      for (StaticPetal p : staticPetals) {
        p.isShattering = false;
      }
      
      // register that the rose is now reconstructed
      reconstructedRose = true;
    }
    
    // reset rose graphics, but not time variables ("r" key)
    if (keyCode == 82) {
      resetGraphics();
    }
  }
  
  // start show ("s" key)
  if (keyCode == 83) {
    startShow();
  }

}
