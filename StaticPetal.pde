
// object for petals that remain stationary throughout the course of the show
class StaticPetal {
  
  float[][] vertices;    // coordinates of all vertices in this petal shape
  color startColor;      // color of the petal at beginning of show
  color endColor;        // color of the petal by the end of show
  int age;               // number of frames this petal has been alive for
  boolean isOutside;     // whether or not this petal is found on the outer ring of petals of the rose (which fade away)
  PVector centroid;      // coordinate of geometric centroid of this shape
  PVector originalCentroid;   // copy of the original centroid position
  boolean isShattering;    // whether or not this static petal should be moving and fading (end of show shatter effect)
  PVector shatterVel;    // randomized shattering velocity of this petal
  PVector reverseVel;  // inverse velocity of shattering velocity
  int shatterAge;
  
  // construct a new StaticPetal
  StaticPetal(float[][] vertices_, color startColor_, color endColor_, boolean isOutside_) {
    this.vertices = verticesCopy(vertices_);
    this.startColor = startColor_;
    this.endColor = endColor_;
    this.isOutside = isOutside_;
    this.centroid = getCentroid(this.vertices);                     // calculate petal centroid from vertices
    this.originalCentroid = this.centroid.copy();
    this.shatterAge = STATIC_PETAL_LIFESPAN;
    this.randomizeShatterVelocity();
  }
  
  // choose random shattering velocity, and calculate its inverse
  void randomizeShatterVelocity() {
    this.shatterVel = new PVector(random(-SHATTER_VELOCITY, SHATTER_VELOCITY), random(-SHATTER_VELOCITY, SHATTER_VELOCITY));    // randomize shatter velocity
    this.reverseVel = this.shatterVel.copy().mult(-1);
  }
  
  // display the petal on the canvas
  void display(float overrideAlpha, boolean override) {
    // fade the rose away, if on outside
    float alpha = 255, maxAlpha = 255;
      
    // if not overriding, proceed as normal
    if (!override) {
      // if petal on outside of rose
      if (this.isOutside && !reconstructedRose) {
        // fade as show progresses
        alpha = map(getTimeSeconds(), SHOW_START, SHOW_END, 255, 0);
        
        // record the highest alpha the shattering effect should shade this petal with
        maxAlpha = alpha;
      }
      
      // scale alpha based on shatter age (but only as high as max alpha, so faded outside petals do not brighten)
      alpha = map(this.shatterAge, STATIC_PETAL_LIFESPAN, 0, maxAlpha, 0);
    } else {
      // override with given alpha
      alpha = overrideAlpha;
    }

    // interpolate between start and end color by fade factor based on current time in show
    fill(lerpColor(this.startColor, this.endColor, colorFadeFactor), alpha);
    
    // display shape of petal
    drawVertices(this.vertices);
  }
  
  // update the age of the petal
  void update() {
    this.age++;  // increment age
    
    // if the rose is currently shattering
    if (this.isShattering && this.shatterAge > 0) {
      // decrease shatter age
      this.shatterAge--;
      
      // apply velocity to centroid
      this.centroid.add(this.shatterVel);
      
      // add velocity to position of petal
      for (int i = 0; i < this.vertices.length; i++) {
        this.vertices[i][0] += this.shatterVel.x;
        this.vertices[i][1] += this.shatterVel.y;
      }
      
      // apply gravity to shatter velocity
      this.shatterVel.add(gravity);
    }
    
    // if not shattering and not at original position
    if (!this.isShattering && (this.originalCentroid.x != this.centroid.x || this.originalCentroid.y != this.centroid.y)) {
      PVector reverse = new PVector(this.originalCentroid.x - this.centroid.x, this.originalCentroid.y - this.centroid.y).mult(SHATTER_REVERSAL_DECEL);
      
      // increment shatter age back to normal
      if (this.shatterAge < STATIC_PETAL_LIFESPAN) {
        this.shatterAge++;
      }
      
      // apply reverse velocity to centroid
      this.centroid.add(reverse);
      
      // add velocity to position of petal
      for (int i = 0; i < this.vertices.length; i++) {
        this.vertices[i][0] += reverse.x;
        this.vertices[i][1] += reverse.y;
      }
    }
  }
}
