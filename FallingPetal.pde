
// object for petals that falling down the screen
class FallingPetal {
  
  float[][][] vertices;          // vertices for all petal panels in this petal
  FallingPetalPanel[] panels;    // objects containing info for panels that compose this falling petal
  PVector avgCentroid;           // coordinate of geometric centroid of this shape
  int age = 0;                   // number of frames this petal has been alive for
  ParticleSystem ps;             // the particle system related to this falling petal
  
  // construct a new FallingPetal
  FallingPetal(float[][][] vertices) {
    // initialize array for panels of this falling petal
    this.panels = new FallingPetalPanel[vertices.length];
    
    // sums for finding average centroid of petal panels
    int sumX = 0, sumY = 0;
    
    // for each petal panel set of vertices
    for (int i = 0; i < vertices.length; i++) {
      // construct a new panel object for this panel, with random living and dead rose colors
      this.panels[i] = new FallingPetalPanel(vertices[i], randomColor(liveRoseColors), randomColor(deadRoseColors));
      
      // factor in this panel's centroid to avg centroid of petal
      sumX += this.panels[i].centroid.x;
      sumY += this.panels[i].centroid.y;
    }
    
    // compute average centroid of all panels
    this.avgCentroid = new PVector((int) Math.floor(sumX / this.panels.length), (int) Math.floor(sumY / this.panels.length));
    
    // create new particle system at the avg centroid of this petal
    this.ps = new ParticleSystem(avgCentroid);
  }
  
  // display the petal on the canvas
  void display() {
    // run the particle system
    ps.run();

    // for each panel in this falling petal
    for (FallingPetalPanel p : this.panels) {
      // choose the fill color for this panel (based on age of petal)
      p.chooseFill(this.age);
      
      // draw the vertices of this panel
      beginShape();
      for (float[] v : p.vertices) {
        vertex(v[0], v[1]);
      }
      endShape();
    }
  }
  
  // update the position & color & age of the petal as it falls
  void update() {
    // if falling petal still alive, keep adding particles
    if (this.age < MAX_PETAL_AGE) {
      // map the starting lifespan of each particle based on petal's age: older the petal, the shorter the starting lifespan of each particle
      ps.addParticle(map(this.age, 0, MAX_PETAL_AGE, PARTICLE_LIFESPAN, 0));
    }

    // start incrementing age of petal after it has passed 3/4 screen height threshold
    if (CENTER_Y + this.avgCentroid.y > CENTER_Y * PETAL_FADE_THRESHOLD) {
      // increment petal age
      this.age++;
    }
    
    // calculate velocity of petal based on vertical position (using sinusoidal function for fall trajectory)
    PVector velocity = new PVector((int) (4 * Math.sin(0.08 * this.avgCentroid.y)), 0.8);
    
    // apply velocity to avg centroid
    this.avgCentroid.add(velocity);
    
    // apply velocity to center of particle system
    this.ps.move(velocity);
    
    // update the positional properties of every panel in this falling petal
    for (FallingPetalPanel p : this.panels) {
      p.update(velocity);
    }
  }
  
  // check that associated particle system is also dead
  boolean particleSystemIsDead() {
    return this.ps.systemIsDead();
  }
}
