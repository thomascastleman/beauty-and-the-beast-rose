
// object for individual panels that comprise FallingPetal objects
class FallingPetalPanel {
  
  float[][] vertices;    // coordinates of all vertices in this petal shape
  PVector centroid;      // coordinate of geometric centroid of this shape
  color startColor;      // color of the petal at the start of show
  color endColor;        // color of petal by the end of show
  
  // construct a new FallingPetal
  FallingPetalPanel(float[][] vertices_, color startColor_, color endColor_) {
    this.vertices = verticesCopy(vertices_);
    this.startColor = startColor_;
    this.endColor = endColor_;
    this.centroid = getCentroid(this.vertices);
  }
  
  // choose the proper fill color for this petal, based on its parent petal's age
  void chooseFill(int age) {
    // interpolate between start and end color by fade factor based on current time in show
    fill(lerpColor(this.startColor, this.endColor, colorFadeFactor), map(age, 0, MAX_PETAL_AGE, 255, 0));
  }
  
  // apply a change in the position of the panel to its centroid and all vertices
  void update(PVector velocity) {
    // apply some shift to all vertices & centroid
    this.centroid.add(velocity);

    // add velocity to position of petal
    for (int i = 0; i < this.vertices.length; i++) {
      this.vertices[i][0] += velocity.x;
      this.vertices[i][1] += velocity.y;
    }
  }
}
