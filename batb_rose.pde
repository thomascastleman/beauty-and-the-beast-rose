
import java.util.*;

/*

  CUE INSTRUCTIONS
    
    S key - show starts (rose fades in from black AFTER opening)
    
    SPACE key - petal falls
    
    D key - cue Beast's death (rose shatters, last petal falls)
    
    T key - cue Beast's transformation (rose reconstructs, sparkles)
    
    R key - emergency reset (rose resets to state before transformation)

*/

int BACKGROUND_COLOR = 0;              // color of background
int CENTER_X, CENTER_Y;                // coordinates of center of window
int SHOW_START, SHOW_END;              // times, in seconds, of show start & show end
int SHOW_DURATION = 8; // 6300;       // estimated duration of show (seconds)
int PETAL_FALL_INTERVAL = 3;           // interval at which petals will fall (seconds)
int MAX_PETAL_AGE = 100;               // duration of falling petal fade (in frames)
float SCALE_FACTOR = 1.0;                // scaling factor of ALL graphics
float PARTICLE_LIFESPAN = 140;         // highest lifespan for a particle (in particle system)
int STATIC_PETAL_LIFESPAN = 200;        // frame lifespan of a static petal as it fades in shatter
float PETAL_FADE_THRESHOLD = 1.5;      // factor multipled by CENTER_Y to get the y-position past which all falling petals will begin to fade (depended on scale)
float MAX_COLOR_FADE = 0.9;            // maximum amount by which colors should fade to dead
float SHATTER_VELOCITY = 1.1;         // highest (& lowest) shatter velocity for static petals
float SHATTER_REVERSAL_DECEL = 0.03;  // rate of deceleration for petals returning to their original position

// gravity as acceleration vector
PVector gravity = new PVector(0, 0.05);

// lists of petals in existence
ArrayList<StaticPetal> staticPetals = new ArrayList<StaticPetal>();
ArrayList<FallingPetal> fallingPetals = new ArrayList<FallingPetal>();
ArrayList<StaticPetal> stemPanels = new ArrayList<StaticPetal>();

float colorFadeFactor = 0;  // factor controlling how much colors fade by, increases as show goes on
boolean showFlowerCover = true;  // whether or not the static petal background cover should be shown

int FADE_IN_TIME = 200;  // time taken in frames to fade in
boolean showStarted = false;
int fadeInStatus = 0;  // status of beginning fade-in

ParticleSystem endGlowPS;  // particle system for glowing on rose (end of show)
boolean reconstructedRose = false;  // has the rose been destroyed and reconstructed yet (end of show)
boolean isFullyReconstructed = false;  // is the rose fully back to its constructed state yet (end of show)

void setup() {
  fullScreen();
  background(BACKGROUND_COLOR);
  noStroke();
  
  // calcalate coordinates of center of screen
  CENTER_X = floor((width / 2));
  CENTER_Y = floor((height / 2));

  // -------------------------------------- debug --------------------------------------  
  //background(210);
  //drawVertices(flowerCoverVertices);
  
  //for (float[][][] v : fallingPetalVertices) {
  //  FallingPetal fp = new FallingPetal(v);
  //  fp.display();
  //}
  // -----------------------------------------------------------------------------------
}

void draw() {  
  // if the show has started (rose has started / finished fading)
  if (showStarted) {
    // check if alpha needs to be overridden for fade
    boolean override = false; float overrideAlpha = 255.0f;
    
    // if still fading, override alpha (display in translucence)
    if (fadeInStatus < FADE_IN_TIME) {
      override = true;
      overrideAlpha = map(fadeInStatus, 0, FADE_IN_TIME, 0, 255);
      fadeInStatus++;
    }
  
    // get the current time in seconds
    int time = getTimeSeconds();
    
    // map how colors should interpolate based on where we are in the show duration
    colorFadeFactor = time < SHOW_END ? map(time, SHOW_START, SHOW_END, 0, MAX_COLOR_FADE) : MAX_COLOR_FADE;
    
    // if rose is reconstructed, ignore time scaling and leave bright colors
    if (reconstructedRose && staticPetals.size() > 0) {
        // scale closeness of petals from being back to full rose to brightness
        colorFadeFactor = map(staticPetals.get(0).shatterAge, 0, STATIC_PETAL_LIFESPAN, 1, 0);
        
        // if rose hasn't yet fully reconstructed, check if it is
        if (!isFullyReconstructed) {
          boolean allPetalsReturned = true;
          // check every petal to see if reached maximum age
          for (int i = 0; i < staticPetals.size(); i++) {
            // if even one petal has not reached full age yet
            if (staticPetals.get(i).shatterAge < STATIC_PETAL_LIFESPAN) {
              // not all petals have made it back yet
              allPetalsReturned = false;
            }
          }
          
          // if all petals have made it back to their original positions, register rose as fully reconstructed
          if (allPetalsReturned) {
            isFullyReconstructed = true;
          }
        }
    }
  
    // draw the background
    background(BACKGROUND_COLOR);
  
    pushMatrix();
      // translate to center and a little bit up
      translate(CENTER_X, CENTER_Y - 35);
      scale(SCALE_FACTOR);
      
      // if rose is fully back to form, start glowing particle effect
      if (reconstructedRose && isFullyReconstructed) {
        endGlowPS.addParticle(500);
        endGlowPS.run();
      }
  
      // draw all the panels that make up the stem
      for (StaticPetal p : stemPanels) {
        p.update();
        p.display(overrideAlpha, override);
      }
      
      // display & update any falling petals that exist, removing if off screen
      ListIterator<FallingPetal> iter = fallingPetals.listIterator();
      while (iter.hasNext()) {
        FallingPetal p = iter.next();
    
        // update and display each note
        p.update();
        p.display();
    
        // if a falling petal is finished, remove it from list
        if ((p.avgCentroid.y > height / 2 || p.age >= MAX_PETAL_AGE) && p.particleSystemIsDead()) {
          iter.remove();
        }
      }
      
      if (showFlowerCover) {
        // draw the cover for inside static petals (prevent falling petals from being seen behind rose)
        fill(lerpColor(liveFlowerCoverColor, deadFlowerCoverColor, colorFadeFactor), overrideAlpha);
        drawVertices(flowerCoverVertices);
      }
      
      // update & display static petals
      for (StaticPetal p : staticPetals) {
        p.update();
        p.display(overrideAlpha, override);
      }
  
    popMatrix();
    
    // ---------------------- uncomment this for regular petal falling ----------------------
  
    //// if time to add new falling petal (and no petal already falling)
    //if (getTimeSeconds() % PETAL_FALL_INTERVAL == 0 && fallingPetals.size() < 1) {      
    //  // construct a falling petal object in this shape
    //  fallingPetals.add(getRandomFallingPetal());
    //}
      
    // --------------------------------------------------------------------------------------
  }
}
